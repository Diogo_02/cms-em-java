package beans;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import controle.UsuarioControle;
import cript.MyCrypto;
import modelo.Usuario;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
// Dando nome ao javaBean
@ManagedBean(name="UserBean")
@RequestScoped
@SessionScoped
/**
 * 
 * @author Diogo
 *
 */
public class UserBean {
	int id;
	String nome, senha;
	
	/**
	 * Retornos dos atributos private
	 * @return encapsulamentos
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	/**
	 * Sele��O de todos os usu�rios 
	 * @return lista de usu�rios(com criptografia)
	 */
	ArrayList<Usuario> lista = new UsuarioControle().consultarUsuario();
	public ArrayList<Usuario> getLista(){
		return lista;
	}
	public void setLista(ArrayList<Usuario> lista) {
		this.lista = lista;
	}	
	/**
	 *	Somente um usu�rio espec�fico 
	 * @param id
	 * @return um item
	 */
	public Usuario retornarItem(int id) {
		return lista.get(id);
	}
	
	/**
	 * Remo��o de usu�rio
	 * @param id
	 * @return p/index
	 */
//	public String deletar(int id) {
//		new UsuarioControle().remover(id);
//		return "index";
//	}
	
	/**
	 * Inser��o de usu�rio
	 * @return
	 */
	public String add() {
		Usuario model = new Usuario();
		UsuarioControle control = new UsuarioControle();
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	        keyGenerator.init(256);
	        SecretKey key = keyGenerator.generateKey();
	        byte[] IV = new byte[16];
	        SecureRandom random = new SecureRandom();
	        random.nextBytes(IV);
	        byte[] nomeCripto = MyCrypto.encrypt(nome.getBytes(),key, IV);
	        String cpN = Base64.getEncoder().encodeToString(nomeCripto);
	        byte[] senhaCripto = MyCrypto.encrypt(senha.getBytes(),key, IV);
	        String cpS = Base64.getEncoder().encodeToString(senhaCripto);
	        String iv = Base64.getEncoder().encodeToString(IV);
	        String kee = Base64.getEncoder().encodeToString(key.getEncoded());
	        model.setNome(cpN);
	        model.setSenha(cpS);
	        model.setIv(iv);
	        model.setKeey(kee);	
	        if(control.add(model) ) {
	        	return "index";
	        }else {
	        	return "erro";
	        }
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return "index";
	}

	/**
	 * Verifica��o e cria��o da sess�o
	 * @return 
	 * @throws Exception
	 */
	public boolean verificar() throws Exception {
		boolean resultado = false;
				Usuario crip = new Usuario();
				UsuarioControle control = new UsuarioControle();
					crip = control.consultaID(1);
			        byte[] tt = Base64.getDecoder().decode(crip.getNome());
			        byte[] t1 = Base64.getDecoder().decode(crip.getSenha());
			        byte[] key = Base64.getDecoder().decode(crip.getKeey());
			        byte[] iv = Base64.getDecoder().decode(crip.getIv());
			        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");
			        System.out.println("teste"+tt+"-"+t1);
					String nomeD = MyCrypto.decrypt(tt,originalKey, iv);
			        String senhaD = MyCrypto.decrypt(t1,originalKey, iv);
			        System.out.println("NOme : "+nomeD+"-"+nome);
			        System.out.println("Senha : "+senhaD+"-"+senha);
			        if(nome.equals(nomeD) && senha.equals(senhaD)) {
			        	if(nome.equals("admin") && senha.equals("admin")) {
			        		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login", nomeD);
				        	System.out.println("Sess�o>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login"));  
				        	FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			        	}
			        	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login", nomeD);
			        	System.out.println("Sess�o>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login"));  
			        	FacesContext.getCurrentInstance().getExternalContext().redirect("pageUser.xhtml");
			        }else {
			        	System.out.println("Usu�rio ou senha incorretos");
			        }
					resultado = true;
		return resultado;
	}
	
	/**
	 * N�o funcionou
	 * @return usu�rios decriptados
	 * @throws Exception
	 */
	public ArrayList<Usuario> decriptar() throws Exception {
		ArrayList<Usuario> lista = this.getLista();
		for (int i = 0; i < 5; i++) {
			Usuario user = new Usuario();
			byte[] tt = Base64.getDecoder().decode(user.getNome());
	        byte[] t1 = Base64.getDecoder().decode(user.getSenha());
	        byte[] key = Base64.getDecoder().decode(user.getKeey());
	        byte[] iv = Base64.getDecoder().decode(user.getIv());
	        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");
	        String nomeD = MyCrypto.decrypt(tt,originalKey, iv);
	        String senhaD = MyCrypto.decrypt(t1,originalKey, iv);
			user.setNome(nomeD);
			user.setSenha(senhaD);
			user.setIv(user.getIv());
			user.setKeey(user.getKeey());
			lista.add(user);
		}
		return lista;
	}
}