package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import controle.CardControle;
import modelo.Card;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Base64;

/**
 * Bean do Card
 * @author Diogo Santos
 *
 */
@ManagedBean(name="CardBean")
public class CardBean {
	public int id;
	public String nome, descricao;
	public Part foto;
	public String folder; 
	
	/**
	 * Encapsulamentos dos atributos
	 * @return get e set
	 */
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Part getFoto() {
		return foto;
	}
	public void setFoto(Part foto) {
		this.foto = foto;
	}
	
	/**
	 * Seleciona todos os dados do card
	 * @return da lista de cards
	 */
	ArrayList<Card> lista = new CardControle().consultarTodos();
	public ArrayList<Card> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Card> lista) {
		this.lista = lista;
	}
	
	public Card retornarItem(int id) {
		return lista.get(id);
	}
	
	/**
	 * Inser��o de dados do card
	 * @return p/ index or p/ erro
	 */

	/**
	 * Seleciona apenas os dados de um card espec�fico
	 * @param id
	 */
	public void carregarId(int id) {
		Card car = new CardControle().consultaUm(id);
		this.setId(car.getId());
		this.setNome(car.getNome());
		this.setDescricao(car.getDescricao());
	}
	
	/**
	 * Edi��o do card 
	 * @return  okay-> p/index or error-> p/erro
	 */
	public String editar() {
		String resultado ="imagem";
		try {
			String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
			InputStream imgs = foto.getInputStream();
			String imgNome = foto.getSubmittedFileName();
			int imgDiv = imgNome.lastIndexOf('.');
			String ext = imgNome.substring(imgDiv);
			folder = Base64.getEncoder().encodeToString(imgNome.getBytes());
			imgNome = folder + ext;
			Files.copy(imgs,new File(realPath+"img", imgNome).toPath(),StandardCopyOption.REPLACE_EXISTING);
			CardControle fotoC = new CardControle();
			Card fotoM = new Card();
			fotoM.setId(id);
			fotoM.setNome(nome);
			fotoM.setDescricao(descricao);
			fotoM.setFoto("img/"+imgNome);
			fotoC.editar(fotoM);
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");			
		}catch(Exception e) {
			e.getMessage();
		}
		return resultado;
			
	}
	/**
	 * Remo��o dos dados do card espec�fico 
	 * @param id
	 * @return p/index
	 */
	public String apagar(int id) {
		if(new CardControle().remover(id)) {
			return "erro";
		}else {
			return "index";
		}
	}
}
