package modelo;

public class Compendio {
	int id;
	String titulo;
	String txt;
	String video;
	public Compendio(int id, String titulo, String txt, String video) {
		// TODO Auto-generated constructor stub
		this.setId(id);
		this.setTitulo(titulo);
		this.setTxt(txt);
		this.setVideo(video);
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTxt() {
		return txt;
	}
	public void setTxt(String txt) {
		this.txt = txt;
	}
	
}
