package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Card;

/**
 * Classe Controle dos cards
 * @author Diogo
 * @return CRUD dos Card
 */
public class CardControle {
	/**
	 * Inser��o com foto e dados do card
	 * @param imagem
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean inserir(Card imagem) throws SQLException{
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = ("INSERT INTO  card(nome,descricao,foto) values(?,?,?) ;");
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, imagem.getNome());
			ps.setString(2, imagem.getDescricao());
			ps.setString(3, imagem.getFoto());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
	}
		return resultado;
	}		
	/**
	 * Consulta de apenas um espec�fico
	 * @param id
	 * @return
	 */
	public Card consultaUm(int id) {
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM card WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Card card = new Card();
			if(rs.next()) {
				card.setId(rs.getInt("id"));
				card.setNome(rs.getString("nome"));
				card.setDescricao(rs.getString("descricao"));
				card.setFoto(rs.getString("foto"));
			}
			
			new Conexao().fecharConexao(con);
			return card;
		}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		return null;
	}	
	/**
	 * Consulta de todos os dados
	 * @return array com dados 
	 */
	public ArrayList<Card> consultarTodos(){
		ArrayList<Card> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM card;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Card>();
				while(rs.next()) {
					Card card = new Card();
					card.setId(rs.getInt("id"));
					card.setNome(rs.getString("nome"));
					card.setDescricao(rs.getString("descricao"));
					card.setFoto(rs.getString("foto"));
					lista.add(card);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Edi��o dos cards
	 * @param imagem
	 * @return
	 */
	public boolean editar(Card imagem) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE card SET nome=?, descricao=?, foto=? WHERE id=?");
			ps.setString(1,imagem.getNome());
			ps.setString(2, imagem.getDescricao());
			ps.setString(3, imagem.getFoto());
			ps.setInt(4, imagem.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o imagem: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	/**
	 * Remo��o de cards
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM card WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		catch(Exception e) {
			e.getMessage();
		}
		return resultado;
	}
}

