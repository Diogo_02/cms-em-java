package modelo;

public class Usuario {
	private int id;
	private String nome;
	private String senha;
	private String iv;
	private String keey;
	
	public String getIv() {
		return iv;
	}
	public void setIv(String iv) {
		this.iv = iv;
	}
	public String getKeey() {
		return keey;
	}
	public void setKeey(String keey) {
		this.keey = keey;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
