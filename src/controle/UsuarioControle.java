package controle;
import modelo.Usuario;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Classe Controle dos Usu�rios 
 * @author Diogo
 * @return CRUD dos usu�rios 
 */
public class UsuarioControle {
	/**
	 * 
	 * @return de todos os usu�rios
	 */
	public ArrayList<Usuario> consultarUsuario(){
		ArrayList<Usuario> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Usuario>();
				while(rs.next()) {
					Usuario user = new Usuario();
					user.setId(rs.getInt("id"));
					user.setNome(rs.getString("nome"));
					user.setSenha(rs.getString("senha"));
					user.setIv(rs.getString("iv"));
					user.setKeey(rs.getString("keey"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	/**
	 * 
	 * @param n
	 * @return Apenas um usuario especifico 
	 * @throws SQLException
	 */
	public Usuario consultaID(int n) throws SQLException{
		Usuario user = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM usuario where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				user = new Usuario();
				while(rs.next()) {
					user.setId(rs.getInt("id"));
					user.setNome(rs.getString("nome"));
					user.setSenha(rs.getString("senha"));
					user.setIv(rs.getString("iv"));
					user.setKeey(rs.getString("keey"));
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return user;
	}
	
	/**
	 * 
	 * @param user
	 * @return m�todo de inser��o
	 */
	public boolean add(Usuario user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("Insert INTO usuario(nome,senha,iv,keey) VALUES (?,?,?,?)");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			ps.setString(3, user.getIv());
			ps.setString(4, user.getKeey());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar registro." + e.getMessage());
		}
		return resultado;
	}
	

//	public boolean remover(int id) {
//		boolean resultado = false;
//		try {
//			Connection con = new Conexao().abrirConexao();
//			PreparedStatement ps = con.prepareStatement("DELETE FROM usuario WHERE id=?");
//			ps.setInt(1, id);
//			if(ps.execute()){
//				resultado = true;
//			}
//			new Conexao().fecharConexao(con);
//		}catch(SQLException e) {
//			System.out.println("Erro no servidor: " + e.getMessage());
//		}
//		return resultado;
//	}
}
