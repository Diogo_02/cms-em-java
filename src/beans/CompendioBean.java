package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import controle.CompendioControle;
import modelo.Compendio;
/**
 * 
 * @author Diogo
 *
 */
@ManagedBean(name="CompendioBean")
public class CompendioBean {
	public int id;
	public String titulo;
	public String txt;
	public Part video;
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part vide) {
		this.video = vide;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTxt() {
		return txt;
	}
	public void setTxt(String txt) {
		this.txt = txt;
	}
	
	public String add() throws SQLException, IOException {
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
		InputStream vid = video.getInputStream();
		String vidNome = video.getSubmittedFileName();
		int vidDiv = vidNome.lastIndexOf('.');
		String ext = vidNome.substring(vidDiv);
		vidNome = titulo + ext;
		Files.copy(vid, new File(realPath+"video", vidNome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Compendio videom = new Compendio(this.getId(),this.getTitulo(),this.getTxt(), "video/"+vidNome);
		CompendioControle videoc= new CompendioControle();
		boolean resultado = videoc.inserir(videom);
		if(resultado) {
			System.out.println("Deu ok");
			return "index";
		}else {
			System.out.println("Erro dnv");
			return "texto";
		}		
	}
	
	ArrayList<Compendio> lista = new CompendioControle().consultarTodos();
	public ArrayList<Compendio> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Compendio> lista) {
		this.lista = lista;
	}
}
