package controle;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Video;

/**
 * Classe Controle dos cards
 * @author Diogo
 * @return CRUD dos Card
 */
public class VideoControle {
	public boolean inserirvideo(Card video) throws SQLException{
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = ("INSERT INTO  card(nome,descricao,foto) values(?,?,?) ;");
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, video.getNome());
			ps.setString(2, video.getDescricao());
			ps.setString(3, video.getFoto());
			if(!ps.execute()) {
				resultado =true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
	}
		return resultado;
	}	
	
	public Card consultaUm(int id) {
		Card video = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM card WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				video = new Card(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"), rs.getString("foto"));		
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return video;
	}
	
	public ArrayList<Card> consultarTodos(){
		ArrayList<Card> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM card;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Card>();
				while(rs.next()) {
					Card video = new Card(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"), rs.getString("foto"));
					lista.add(video);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean editar(Card video) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE card SET nome=?, descricao=?, foto=? WHERE id=?");
			ps.setString(1,video.getNome());
			ps.setString(2, video.getDescricao());
			ps.setString(3, video.getFoto());
			ps.setInt(4, video.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o video: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM card WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		catch(Exception e) {
			e.getMessage();
		}
		return resultado;
	}
}

