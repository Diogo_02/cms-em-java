package beans;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="Deslogar")
@RequestScoped
@SessionScoped
/**
 * 
 * @author Diogo
 *
 */
public class Deslogar {
	/**
	 * Apaga a sess�o
	 * @return boolean
	 */
	public boolean deslogar() {
		boolean a = true;
		if(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login")!=null) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("login");
			try {
				System.out.println("Sess�o::::::"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login"));
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return a;
	}
	
}
