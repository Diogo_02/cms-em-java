package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import controle.CardControle;
import modelo.Card;

@ManagedBean(name="CardAdd")

public class CardAdd {
	public int id;
	public String nome;
	public String descricao;
	public Part foto;
	
	
	public Part getFoto() {
		return foto;
	}


	public void setFoto(Part foto) {
		this.foto = foto;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public String inserir() throws SQLException, IOException{
		String resultado = "card";
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
		InputStream img = foto.getInputStream();
		String imgNome = foto.getSubmittedFileName();
		int imgDiv = imgNome.lastIndexOf('.');
		String ext = imgNome.substring(imgDiv);
		imgNome = nome + ext;
		Files.copy(img, new File(realPath+"img/", imgNome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Card fotom = new Card();
		CardControle fotoc= new CardControle();
		fotom.setId(id);
		fotom.setDescricao(descricao);
		fotom.setNome(nome);
		fotom.setFoto("img/"+imgNome);
		fotoc.inserir(fotom);
		FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		return resultado;
		
	}
}