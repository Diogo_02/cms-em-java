package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Compendio;;

/**
 * 
 * @author Diogo
 *
 */
public class CompendioControle {
	/**
	 * Inser��o de conte�do do compendio
	 * @param video
	 * @return
	 * @throws SQLException
	 */
	public boolean inserir(Compendio video) throws SQLException{
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = ("INSERT INTO sobre(titulo,txt,video) values(?,?,?) ;");
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, video.getTitulo());
			ps.setString(2, video.getTxt());
			ps.setString(3, video.getVideo());
			if(!ps.execute()) {
				resultado =true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
	}
		return resultado;
	}
	
	/**
	 * Consulta de todos os dados
	 * @return lista
	 */
	public ArrayList<Compendio> consultarTodos(){
		ArrayList<Compendio> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM sobre;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Compendio>();
				while(rs.next()) {
					Compendio imagem = new Compendio(rs.getInt("id"),rs.getString("titulo"),rs.getString("txt"), rs.getString("video"));
					lista.add(imagem);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	/**
	 * edi��o de dados
	 * @param imagem
	 * @return
	 */
	public boolean editar(Compendio imagem) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE sobre SET titulo=?, txt=?, video=? WHERE id=?");
			ps.setString(1,imagem.getTitulo());
			ps.setString(2, imagem.getTxt());
			ps.setString(3, imagem.getVideo());
			ps.setInt(4, imagem.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o imagem: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
}
