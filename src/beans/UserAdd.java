package beans;

import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.faces.bean.ManagedBean;
import modelo.Usuario;
import controle.UsuarioControle;
import cript.MyCrypto;

@ManagedBean(name="UserAdd")
public class UserAdd {
	int id;
	String nome, senha,iv,keey;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getIv() {
		return iv;
	}
	public void setIv(String iv) {
		this.iv = iv;
	}
	
	public String getKeey() {
		return keey;
	}
	public void setKeey(String keey) {
		this.keey = keey;
	}
	
	/**
	 * cadastro com criptografia
	 * @return  for index or erro
	 */
	public String add() {
		Usuario model = new Usuario();
		UsuarioControle control = new UsuarioControle();
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	        keyGenerator.init(256);
	        SecretKey key = keyGenerator.generateKey();
	        byte[] IV = new byte[16];
	        SecureRandom random = new SecureRandom();
	        random.nextBytes(IV);
	        byte[] nomeCripto = MyCrypto.encrypt(nome.getBytes(),key, IV);
	        String cpN = Base64.getEncoder().encodeToString(nomeCripto);
	        byte[] senhaCripto = MyCrypto.encrypt(senha.getBytes(),key, IV);
	        String cpS = Base64.getEncoder().encodeToString(senhaCripto);
	        String iv = Base64.getEncoder().encodeToString(IV);
	        String kee = Base64.getEncoder().encodeToString(key.getEncoded());
	        model.setNome(cpN);
	        model.setSenha(cpS);
	        model.setIv(iv);
	        model.setKeey(kee);	
	        if(control.add(model) ) {
	        	return "index";
	        }else {
	        	return "erro";
	        }
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return "index";
	}
}
